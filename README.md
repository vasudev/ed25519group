# Ed25519 Twisted Edwards Curve Group #

This package implements group operation for Twisted Edwards Curve form of
Curve25519. This package is mainly written to support the Go implementation of
[SPAKE2 algorithm
](http://www.di.ens.fr/%7Epointche/Documents/Papers/2005_rsa.pdf).

This package uses following Python and Haskell implementation of the same group as
reference. 

  * [Python Implementation of Ed25519
    Group](https://github.com/warner/python-spake2/raw/master/src/spake2/ed25519_basic.py)
  * [Haskell Implementation of
    Ed25519](https://github.com/jml/haskell-spake2/raw/master/src/Crypto/Spake2/Groups/Ed25519.hs)


Type `Ed25519` implements the `Group` interface defined in
[gospake2](https://salsa.debian.org/vasudev/gospake2). Rest of operations
defined in this group are generic and can be reused. This module relies on
`math/big` package from *Go Standard library* and is **not optimized for speed.**

## License ##
Copyright: 2018, Vasudev Kamath <vasudev@copyninja.info>

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Library General Public License for more details.

You should have received a copy of the GNU Library General Public License along
with this library; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA


